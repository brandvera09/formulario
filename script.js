
      function validadatos(){
        //variables utilizadas
        var nom = document.formulario.nombre;
        var ape = document.formulario.apellido;
        var dni = document.formulario.cedula;
        var sel = document.formulario.seleccionar;
        var tel = document.formulario.telf;
        var cor = document.formulario.email;
        var opciones = document.getElementsByName("customRadioInline1");
        var seleccionado = false;
        var bot = document.formulario.boton;

        //valida el que el campo nombre tenga caracteres
        if (nom.value == "") {
          alert("Ingrese un nombre en el campo correspondiente")
          return false;
        }
        //valida el que el campo apellido tenga caracteres
        if (ape.value == "") {
          alert("Ingrese un apellido en el campo correspondiente")
          return false;
        }
        //valida el que el campo documento de identidad tenga caracteres 
        if (dni.value == "") {
          alert("Ingrese un documento de identidad en el campo correspondiente")
          return false;
        }
        //valida el que el campo tipo de documento haya sido selecionado
        if (sel.selectedIndex == 0) {
          alert("Seleccione tipo de documento de identidad en la lista desplegable")
          return false;
        }
        //valida el que el campo telefono tenga caracteres 
        if (tel.value == "") {
          alert("Ingrese un telefono en el campo correspondiente")
          return false;
        }
        //valida el que el campo correo tenga caracteres 
        if (cor.value == "") {
          alert("Ingrese un correo en el campo correspondiente")
          return false;
        }
        //valida el que el radio haya sido seleccionado
        for(var i=0; i<opciones.length; i++) {
          if(opciones[i].checked) {
            seleccionado = true;
            break;
          }
        }
        //si no a sido seleccionado el radio
        if(!seleccionado) {
          alert("Seleccione un genero en el campo correspondiente")
          return false;
        }
        //valida el que el campo documento de identidad sea solo de numeros
        for(i=0; i<dni.value.length; i++){
          if(isNaN(parseInt(dni.value.charAt(i)))==true){
            alert("Escriba solo numeros en el campo de documento de identidad");
            return false;
          }
        }
        //se envia el formulario si todos los campos estan llenos
        if(bot != 0){
          alert("Su informacion es correcta, presione aceptar para guardar sus datos");
          return true;
        }
      }

